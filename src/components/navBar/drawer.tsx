import React, { useMemo } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import HomeIcon from "@material-ui/icons/Home";
import ImageIcon from "@material-ui/icons/Image";
import ContactMailIcon from "@material-ui/icons/ContactMail";
import PersonIcon from "@material-ui/icons/Person";
import DescriptionRoundedIcon from "@material-ui/icons/DescriptionRounded";
import { toggleDrawer as toggleDrawerRedux } from "./redux/actions";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  list: {
    width: 250
  },
  fullList: {
    width: "auto"
  },
  links: {
    textDecoration: "none",
    color: "black"
  }
});

export interface PropTypes {
  language: any;
  isDrawerOpen: Boolean;
  toggleDrawerRedux: Function;
}

const Drawer = (props: PropTypes) => {
  const classes = useStyles();
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false
  });

  type DrawerSide = "top" | "left" | "bottom" | "right";
  const toggleDrawer = (side: DrawerSide, open: boolean) => (
    event: React.KeyboardEvent | React.MouseEvent
  ) => {
    if (
      event &&
      event.type === "keydown" &&
      ((event as React.KeyboardEvent).key === "Tab" ||
        (event as React.KeyboardEvent).key === "Shift")
    ) {
      return;
    }
    setState({ ...state, [side]: open });
    props.toggleDrawerRedux(open);
  };

  useMemo(() => {
    let side = "left";
    setState({ ...state, [side]: props.isDrawerOpen });
    // eslint-disable-next-line
  }, [props.isDrawerOpen]);

  const sideList = (side: DrawerSide) => (
    <div
      className={classes.list}
      role="presentation"
      onClick={toggleDrawer(side, false)}
      onKeyDown={toggleDrawer(side, false)}
    >
      <List>
        <Link className={classes.links} to="/">
          <ListItem button>
            <ListItemIcon>{<HomeIcon />}</ListItemIcon>
            <ListItemText primary={props.language ? "خانه" : "Home"} />
          </ListItem>
        </Link>
        <Link className={classes.links} to="/gallery">
          <ListItem button>
            <ListItemIcon>{<ImageIcon />}</ListItemIcon>
            <ListItemText primary={props.language ? "گالری" : "Gallery"} />
          </ListItem>
        </Link>

        <Link className={classes.links} to="/articles">
          <ListItem button>
            <ListItemIcon>{<DescriptionRoundedIcon />}</ListItemIcon>
            <ListItemText primary={props.language ? "مقالات" : "Article"} />
          </ListItem>
        </Link>
        <Link className={classes.links} to="/contactMe">
          <ListItem button>
            <ListItemIcon>{<ContactMailIcon />}</ListItemIcon>
            <ListItemText
              primary={props.language ? "ارتباط با من" : "ContactMe"}
            />
          </ListItem>
        </Link>
        <Link className={classes.links} to="/aboutMe">
          <ListItem button>
            <ListItemIcon>{<PersonIcon />}</ListItemIcon>
            <ListItemText primary={props.language ? "درباره من" : "AboutMe"} />
          </ListItem>
        </Link>
      </List>

      {/* <Divider />
      <List>
        {['All mail', 'Trash', 'Spam'].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List> */}
    </div>
  );

  return (
    <SwipeableDrawer
      open={state.left}
      onClose={toggleDrawer("left", false)}
      onOpen={toggleDrawer("left", true)}
    >
      {sideList("left")}
    </SwipeableDrawer>
  );
};

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    isDrawerOpen: state.navbar.toggleDrawer.isDrawerOpen
  };
};

const mapDispatchToProps = (dispatch: any) => ({
  toggleDrawerRedux: (open: boolean | null) => dispatch(toggleDrawerRedux(open))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Drawer);
