import React, { useEffect, useRef } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { fade, makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { AppBar, Toolbar, IconButton, InputBase, Badge, MenuItem, Menu, Button } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import MenuIcon from "@material-ui/icons/Menu";
import SUNICON from "@material-ui/icons/WbSunnyRounded";
import SearchIcon from "@material-ui/icons/Search";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MoreIcon from "@material-ui/icons/MoreVert";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import HomeIcon from "@material-ui/icons/Home";
import ImageIcon from "@material-ui/icons/Image";
import ContactMailIcon from "@material-ui/icons/ContactMail";
import PersonIcon from "@material-ui/icons/Person";
import DescriptionRoundedIcon from "@material-ui/icons/DescriptionRounded";
import { Link } from "react-router-dom";
import Tooltip from "@material-ui/core/Tooltip";
import LanguageIcon from "@material-ui/icons/Language";
import NavigationIcon from "@material-ui/icons/Navigation";

import Drawer from "./drawer";
import { deviceSize, changeLanguage, toggleDrawer, changeTheme } from "./redux/actions";
import { logout } from "./../Profile/redux/action";
import FlareComponent from "flare-react";
const flare1 = require("./assets/theme.flr");

const NavBar = (props: any) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState<null | HTMLElement>(null);
  const themeColorLogic = useSelector((state: any) => state.navbar.themeColor);
  const [animation, setAnimation] = React.useState<string>("day_idle");
  const dispatch = useDispatch();
  let latestProps = useRef(props);
  const onWindowResize = () => {
    const { deviceSize } = latestProps.current;
    deviceSize(window.innerWidth, window.innerHeight);
  };
  useEffect(() => {
    const runOnce = () => {
      window.addEventListener("resize", onWindowResize, false);
    };
    runOnce();
  }, []);

  useEffect(() => {
    if (themeColorLogic) {
      setTimeout(() => {
        setAnimation("day_idle");
      }, 200);
      setAnimation("switch_day");
    } else {
      setTimeout(() => {
        setAnimation("night_idle");
      }, 200);
      setAnimation("switch_night");
    }
  }, [themeColorLogic]);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  function handleRedirectToLogin(event: React.MouseEvent<HTMLElement>) {
    // setAnchorEl(event.currentTarget);
    if (!props.auth.isLogged) {
      props.history.push("/profile");
    } else {
      // props.history.push('/');
      setAnchorEl(event.currentTarget);
    }
  }

  function handleThemeChange() {
    dispatch(changeTheme());
  }

  function handleMobileMenuClose() {
    setMobileMoreAnchorEl(null);
  }

  function handleMenuClose() {
    setAnchorEl(null);
    handleMobileMenuClose();
  }

  function handleMobileMenuOpen(event: React.MouseEvent<HTMLElement>) {
    setMobileMoreAnchorEl(event.currentTarget);
  }

  const menuId = "primary-search-account-menu";
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem
        onClick={() => {
          props.history.push("/profile");
          handleMenuClose();
        }}
      >
        {props.language ? "پروفایل" : "Profile"}
      </MenuItem>
      <MenuItem
        onClick={() => {
          dispatch(logout());
          handleMenuClose();
          props.history.push("/");
        }}
      >
        {props.language ? "خروج" : "logout"}
      </MenuItem>
    </Menu>
  );

  const mobileMenuId = "primary-search-account-menu-mobile";
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem onClick={handleThemeChange}>
        <IconButton color="inherit">
          <Badge color="secondary">
            <SUNICON />
          </Badge>
        </IconButton>
        <p>{props.language ? "تغییر رنگ قالب" : "change theme color "}</p>
      </MenuItem>

      <MenuItem
        onClick={() => {
          props.changeLanguage();
          handleMenuClose();
        }}
      >
        <IconButton aria-label="change language" color="inherit">
          <Badge color="secondary">
            <LanguageIcon />
          </Badge>
        </IconButton>
        <p>{props.language ? "تغییر زبان به انگلیسی" : "change language to persian "}</p>
      </MenuItem>
      <MenuItem onClick={handleRedirectToLogin}>
        <IconButton aria-label="account of current user" aria-controls="primary-search-account-menu" aria-haspopup="true" color="inherit">
          <AccountCircle />
        </IconButton>
        <p>{props.language ? "حساب کاربری" : "Profile"}</p>
      </MenuItem>
    </Menu>
  );

  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar className={classes.toolBar}>
          {props.width < 959 ? (
            <>
              <IconButton onClick={() => props.toggleDrawer()} edge="start" className={classes.menuButton} color="inherit" aria-label="open drawer">
                <MenuIcon />
              </IconButton>
              <div className={classes.search}>
                <div className={classes.searchIcon}>
                  <SearchIcon />
                </div>
                <InputBase
                  placeholder="Search…"
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput
                  }}
                  inputProps={{ "aria-label": "search" }}
                />
              </div>
            </>
          ) : (
            <>
              <div className={classes.search}>
                <div className={classes.searchIcon}>
                  <SearchIcon />
                </div>
                <InputBase
                  placeholder={props.language ? "جستجو..." : "Search…"}
                  classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput
                  }}
                  inputProps={{ "aria-label": "search" }}
                />
              </div>
              <div>
                {" "}
                <List style={{ display: "flex" }}>
                  <Link className={classes.links} to="/">
                    <ListItem button>
                      <ListItemIcon>{<HomeIcon />}</ListItemIcon>
                      <ListItemText
                        primary={props.language ? "خانه" : "Home"}
                        style={{
                          // color: "white",
                          marginRight: props.language ? "-19px" : "0px",
                          marginLeft: props.language ? "0px" : "-19px",
                          marginTop: "6px"
                        }}
                      />
                    </ListItem>
                  </Link>
                  <Link className={classes.links} to="/gallery">
                    <ListItem button>
                      <ListItemIcon>{<ImageIcon />}</ListItemIcon>
                      <ListItemText
                        primary={props.language ? "گالری" : "Gallery"}
                        style={{
                          // color: "white",
                          marginRight: props.language ? "-19px" : "0px",
                          marginLeft: props.language ? "0px" : "-19px",
                          marginTop: "6px"
                        }}
                      />
                    </ListItem>
                  </Link>

                  <Link className={classes.links} to="/articles">
                    <ListItem button>
                      <ListItemIcon>{<DescriptionRoundedIcon />}</ListItemIcon>
                      <ListItemText
                        primary={props.language ? "مقالات" : "Article"}
                        style={{
                          // color: "white",
                          marginRight: props.language ? "-19px" : "0px",
                          marginLeft: props.language ? "0px" : "-19px",
                          marginTop: "6px"
                        }}
                      />
                    </ListItem>
                  </Link>
                  <Link className={classes.links} to="/contactMe">
                    <ListItem button>
                      <ListItemIcon>{<ContactMailIcon />}</ListItemIcon>
                      <ListItemText
                        primary={props.language ? "ارتباط با من" : "ContactMe"}
                        style={{
                          // color: "white",
                          marginRight: props.language ? "-19px" : "0px",
                          marginLeft: props.language ? "0px" : "-19px",
                          marginTop: "6px"
                        }}
                      />
                    </ListItem>
                  </Link>
                  <Link className={classes.links} to="/aboutMe">
                    <ListItem button>
                      <ListItemIcon>{<PersonIcon />}</ListItemIcon>
                      <ListItemText
                        primary={props.language ? "درباره من" : "AboutMe"}
                        style={{
                          // color: "white",
                          marginRight: props.language ? "-19px" : "0px",
                          marginLeft: props.language ? "0px" : "-19px",
                          marginTop: "6px"
                        }}
                      />
                    </ListItem>
                  </Link>
                </List>
              </div>
            </>
          )}

          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <div style={{ display: "flex", alignItems: "center", cursor: "pointer", justifyContent: "end" }}>
              <IconButton style={{ backgroundColor: "transparent", width: 70, height: 35, padding: 0 }} onClick={handleThemeChange} color="inherit">
                <FlareComponent width={80} height={40} animationName={animation} file={flare1} />
              </IconButton>
            </div>
            <Tooltip title={props.language ? "تغییر زبان به انگلیسی" : "change language to persian"}>
              <IconButton onClick={() => props.changeLanguage()} aria-label="change language" color="inherit">
                <Badge color="secondary">
                  <LanguageIcon />
                </Badge>
              </IconButton>
            </Tooltip>
            {/* <Tooltip title={props.language ? "حساب کاربری" : "profile"}>
              <IconButton
                edge="end"
                aria-label="account of current user"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleRedirectToLogin}
                color="inherit"
              >
                {!props.auth.isLogged ? (
                  <Fab variant="extended">
                    <NavigationIcon className={classes.extendedIcon} />
                    {props.language ? "ورود" : "Login"}
                  </Fab>
                ) : (
                  <AccountCircle />
                )}
              </IconButton>
            </Tooltip> */}
            <Tooltip title={props.language ? "حساب کاربری" : "profile"}>
              <span
                className={classes.profileFather}
                aria-label="account of current user"
                aria-controls={menuId}
                aria-haspopup="true"
                onClick={handleRedirectToLogin}
                color="inherit"
              >
                {!props.auth.isLogged ? (
                  <Button style={{ backgroundColor: "transparent", color: "white" }}>
                    <NavigationIcon className={classes.extendedIcon} />
                    {props.language ? "ورود" : "Login"}
                  </Button>
                ) : (
                  <AccountCircle />
                )}
              </span>
            </Tooltip>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton aria-label="show more" aria-controls={mobileMenuId} aria-haspopup="true" onClick={handleMobileMenuOpen} color="inherit">
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      <Drawer />
      {renderMobileMenu}
      {renderMenu}
    </div>
  );
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    toolBar: {
      backgroundColor: "gray"
    },
    grow: {
      flexGrow: 1
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    title: {
      display: "none",
      [theme.breakpoints.up("sm")]: {
        display: "block"
      }
    },
    search: {
      // marginBottom:'30px',
      position: "relative",
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      "&:hover": {
        backgroundColor: fade(theme.palette.common.white, 0.25)
      },
      marginRight: theme.spacing(1),
      marginLeft: 0,
      width: "170px",
      [theme.breakpoints.up("sm")]: {
        width: "auto"
      }
    },
    profileFather: {
      display: "flex",
      alignItems: "center",
      cursor: "pointer"
    },
    searchIcon: {
      width: theme.spacing(7),
      height: "100%",
      position: "absolute",
      top: 0,
      pointerEvents: "none",
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    },
    inputRoot: {
      color: "inherit "
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 7),
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("sm")]: {
        width: 70,
        "&:focus": {
          width: 130
        }
      }
    },
    sectionDesktop: {
      display: "none",
      [theme.breakpoints.up("md")]: {
        display: "flex"
      }
    },
    sectionMobile: {
      display: "flex",
      [theme.breakpoints.up("md")]: {
        display: "none"
      }
    },

    links: {
      textDecoration: "none",
      backgroundColor: "gray",
      color: "white",
      borderRadius: "10%",
      "&:hover": {
        backgroundColor: "white",
        color: "black",
        "& svg": {
          fill: "black",
          color: "black"
        }
      },

      "& svg": {
        fill: "white",
        color: "white"
      }
    },
    extendedIcon: {
      marginRight: theme.spacing(1)
    }
  })
);

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    width: state.navbar.resize.width,
    language,
    auth: state.auth.auth,
   
  };
};

const mapDispatchToProps = (dispatch: any) => ({
  deviceSize: (width: number, height: number) => dispatch(deviceSize(width, height)),
  changeLanguage: () => dispatch(changeLanguage()),
  toggleDrawer: () => dispatch(toggleDrawer())
});

export default connect(mapStateToProps, mapDispatchToProps)(React.memo(withRouter(NavBar)));
