export const deviceSize = (width: number, height: number) => ({
  type: "DEVICE_RESIZE",
  width,
  height
});

export const changeLanguage = () => ({
  type: "CHANGE_LANGUAGE"
});

export const changeTheme = () => ({
  type: "CHANGE_THEME"
});

export const toggleDrawer = (isOpen: boolean | null = null) => ({
  isOpen,
  type: "TOGGLE_DRAWER"
});
