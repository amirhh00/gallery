import { combineReducers } from "redux";

const initialStateResize = {
  width: window.innerWidth,
  height: window.innerHeight
};

const initialStateLang = {
  direction: "rtl",
  language: "FA"
};

const initialStateDrawer = {
  isDrawerOpen: false
};

const resize = (state = initialStateResize, action: any) => {
  switch (action.type) {
    case "DEVICE_RESIZE": {
      return {
        ...state,
        width: action.width,
        height: action.height
      };
    }

    default:
      return state;
  }
};

const toggleLanguage = (state = initialStateLang, action: any) => {
  switch (action.type) {
    case "CHANGE_LANGUAGE": {
      let { direction, language } = state;

      if (language === "FA") {
        language = "EN";
        direction = "ltr";
      } else {
        language = "FA";
        direction = "rtl";
      }

      return {
        ...state,
        language,
        direction
      };
    }

    default:
      return state;
  }
};

const theme = (state = true, action: any) => {
  switch (action.type) {
    case "CHANGE_THEME": {
      return !state;
    }

    default:
      return state;
  }
};

const toggleDrawer = (state = initialStateDrawer, action: any) => {
  switch (action.type) {
    case "TOGGLE_DRAWER": {
      const { isDrawerOpen } = state;
      const { isOpen } = action;
      if (isOpen === null) {
        return {
          isDrawerOpen: !isDrawerOpen
        };
      } else {
        return {
          isDrawerOpen: isOpen
        };
      }
    }

    default:
      return state;
  }
};

export default combineReducers({ resize, toggleLanguage, toggleDrawer, themeColor: theme });
