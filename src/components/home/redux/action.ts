import axios from "axios";

export const getBlogs = (page = 1, per_page = 10) => async (dispatch: any) => {
  try {
    let response = await axios({
      url: `${process.env.REACT_APP_DOMAIN}/V1/blog?page=${page}&per_page=${per_page}`,
      method: "GET"
    });
    if (response.data.success) {
      dispatch({
        type: "GET_BLOGS",
        payload: response.data
      });
    } else {
      console.log("response", response);
    }
  } catch (err) {
    console.log(err);
  }
};

export const updateSingleBlog = (id:any, title:any, content:any, blogImage = null, updatedAt:any, page = 1) => ({
  type: "UPDATE_SINGLE_BLOG",
  id,
  title,
  content,
  blogImage,
  updatedAt,
  page
});

export const deleteSingleBlog = (id:any, page = 1) => ({
  type: "DELETE_SINGLE_BLOG",
  id,
  page
});
