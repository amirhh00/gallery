import { combineReducers } from "redux";

const initialStateResize = {
  pages: [
    {
      currentBlogs: []
    }
  ],
  blogsCount: 0,
  pagesCount: 1,
  perPage: 10
};

const blogs = (state = initialStateResize, action:any) => {
  switch (action.type) {
    case "GET_BLOGS": {
      const { currentPage, blogs, numOfResults, perPage, pages } = action.payload;
      state.pages[currentPage - 1] = blogs;
      return {
        ...state,
        blogsCount: numOfResults,
        pagesCount: pages,
        perPage: perPage
      };
    }

    case "UPDATE_SINGLE_BLOG": {
      const { id, title, content, blogImage, updatedAt, page } = action;
      state.pages[page] = (state.pages[page] as any).map((value:any) =>
        value._id === id ? { ...value, title, body: content, BlogImage: blogImage, updatedAt } : value
      );
      return JSON.parse(JSON.stringify(state)); // for some f**ed up reason this works!! https://github.com/rt2zz/redux-persist/issues/897#issuecomment-452200271
    }

    case "DELETE_SINGLE_BLOG": {
      const { id, page } = action;
      state.pages[page] = (state.pages[page] as any).filter((value:any) => value._id !== id);
      return JSON.parse(JSON.stringify(state)); // for some f**ed up reason this works!! https://github.com/rt2zz/redux-persist/issues/897#issuecomment-452200271
    }

    default:
      return state;
  }
};

export default combineReducers({ blogs });
