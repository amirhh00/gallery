import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { makeStyles } from "@material-ui/core/styles";
import ImageGallery from "react-image-gallery";
import "react-image-gallery/styles/scss/image-gallery.scss";
import { Container, Box, Typography, CircularProgress, Button, TablePagination } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import { getBlogs } from "./redux/action";
import ArticleOption from "./articleOption";
import { Link } from "react-router-dom";

import news from "./assets/1.jpg";
import portrait from "./assets/2.jpg";
import industrial from "./assets/3.jpg";
import landScape from "./assets/4.jpg";

const images = [
  {
    original: "https://picsum.photos/id/1018/1000/600/",
    thumbnail: "https://picsum.photos/id/1018/250/150/"
  },
  {
    original: "https://picsum.photos/id/1015/1000/600/",
    thumbnail: "https://picsum.photos/id/1015/250/150/"
  },
  {
    original: "https://picsum.photos/id/1019/1000/600/",
    thumbnail: "https://picsum.photos/id/1019/250/150/"
  },
  {
    original: "https://picsum.photos/id/1008/1000/600/",
    thumbnail: "https://picsum.photos/id/1008/250/150/"
  },
  {
    original: "https://picsum.photos/id/1025/1000/600/",
    thumbnail: "https://picsum.photos/id/1025/250/150/"
  },
  {
    original: "https://picsum.photos/id/1016/1000/600/",
    thumbnail: "https://picsum.photos/id/1016/250/150/"
  }
];

const Home = (props: any) => {
  const [jsonstate, setjsonstate] = useState<Array<any>>([]);
  const classes = useStyles(props);
  const [page, setPage] = React.useState(1);
  // const prevpage = usePrevious(page);
  const [rowsPerPage, setRowsPerPage] = React.useState(6);
  // const [rows, setRows] = React.useState([]);

  // const dispatch = useDispatch();
  useEffect(() => {
    props.getBlogs(page, rowsPerPage);
    // eslint-disable-next-line
  }, [page, rowsPerPage]);

  useEffect(() => {
    // dispatch(checkToken);
  }, []);

  const handleChangePage = (_event: any, newPage: number) => {
    setPage(newPage + 1);
  };

  const handleChangeRowsPerPage = (event: { target: { value: React.ReactText } }) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  // useEffect(() => {
  //   props.getBlogs(1, 6);
  //   // eslint-disable-next-line
  // }, []);

  useEffect(() => {
    setjsonstate(props.blogs.pages[0]);
    // console.log('props.blogs.pages',props.blogs.pages);
  }, [props.blogs]);

  // useEffect(() => {
  //   props.getBlogs(1, 6);
  //   // eslint-disable-next-line
  // }, []);

  useEffect(() => {
    setjsonstate(props.blogs.pages[0]);
    // console.log('props.blogs.pages',props.blogs.pages);
  }, [props.blogs]);

  if (jsonstate.length > 0) {
    return (
      <Container maxWidth="lg">
        <Box className={classes.topSection}>
          <Box className={classes.mainTitle}>
            <Typography className={classes.mainTitle_typo}>
              {props.language ? "گالری عکس های شخصی و آموزش عکاسی" : "Gallery of personal photos and teaching photography"}{" "}
            </Typography>
          </Box>

          <Box className={classes.subTitle}>
            <Typography className={classes.subTitle_typo}>
              {props.language
                ? "امین کشاورزی عکاس و خبر نگار ماهنامه افق شهر قدس"
                : "Amin keshavarzi photographer and Journalist for Ofogh Shahr Ghods monthly news agency  "}
            </Typography>
          </Box>
        </Box>
        <Box className={classes.imageSlieder}>
          <ImageGallery items={images} />
        </Box>
        <Box className={classes.Gallery}>
          <Box className={classes.galleryName_father}>
            <Typography className={classes.galleryName}>{props.language ? "گالری عکس ها" : "Gallery"}</Typography>
          </Box>

          <Box className={classes.sectionsFather}>
            <Box className={classes.nameSection}>
              <Box className={classes.imageSections}>
                <Avatar alt="News" src={news} className={classes.img} />
              </Box>

              <Link to="/archive" style={{ textDecoration: "none" }}>
                <Box className={classes.nameTypes}>
                  <Button variant="outlined" className={classes.button}>
                    {props.language ? "خبری" : "news"}
                  </Button>
                </Box>
              </Link>
            </Box>

            <Box className={classes.nameSection}>
              <Box className={classes.imageSections}>
                <Avatar alt="landScape" src={landScape} className={classes.img} />
              </Box>

              <Link to="/archive" style={{ textDecoration: "none" }}>
                <Box className={classes.nameTypes}>
                  <Button variant="outlined" className={classes.button}>
                    {props.language ? "طبیعت" : "landScape"}
                  </Button>
                </Box>
              </Link>
            </Box>

            <Box className={classes.nameSection}>
              <Box className={classes.imageSections}>
                <Avatar alt="portrait" src={portrait} className={classes.img} />
              </Box>

              <Link to="/archive" style={{ textDecoration: "none" }}>
                <Box className={classes.nameTypes}>
                  <Button variant="outlined" className={classes.button}>
                    {props.language ? "پرتره" : "portrait"}
                  </Button>
                </Box>
              </Link>
            </Box>

            <Box className={classes.nameSection}>
              <Box className={classes.imageSections}>
                <Avatar alt="industrial" src={industrial} className={classes.img} />
              </Box>

              <Link to="/archive" style={{ textDecoration: "none" }}>
                <Box className={classes.nameTypes}>
                  <Button variant="outlined" className={classes.button}>
                    {props.language ? "صنعتی" : "industrial"}
                  </Button>
                </Box>
              </Link>
            </Box>
          </Box>
        </Box>

        <Box className={classes.articles}>
          <Box className={classes.articlesTitle}>
            <Typography style={{ fontSize: "1.7em" }}>{props.language ? "مقالات آموزشی" : "Articles"}</Typography>
          </Box>
        </Box>

        <Box className={classes.articlesType}>
          {jsonstate.map((value, index) => {
            return <ArticleOption key={index} data={value} />;
          })}
        </Box>

        <TablePagination
          rowsPerPageOptions={[6]}
          dir={props.language ? "rtl" : "ltr"}
          component="div"
          count={props.blogs.blogsCount}
          rowsPerPage={rowsPerPage}
          page={page - 1}
          backIconButtonProps={{
            "aria-label": "previous page"
          }}
          nextIconButtonProps={{
            "aria-label": "next page"
          }}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Container>
    );
  } else {
    return (
      <div>
        <CircularProgress className={classes.progress} />
      </div>
    );
  }
};

const useStyles = makeStyles((theme: any) => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    width: "100%",
    borderLeft: "5px solid black",
    borderRight: "5px solid black"
  },
  topSection: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    marginTop: "40px",
    justifyContent: "center",
    alignItems: "center"
  },
  mainTitle_typo: {
    fontSize: "2.4em",
    fontWeight: "bold",
    [theme.breakpoints.down(525)]: {
      fontSize: "2em"
    },
    [theme.breakpoints.down(451)]: {
      fontSize: "1.7em"
    },
    [theme.breakpoints.down(382)]: {
      fontSize: "1.5em"
    },
    [theme.breakpoints.down(339)]: {
      fontSize: "1.2em"
    }
  },

  subTitle_typo: {
    fontSize: "1.5em",
    [theme.breakpoints.down(525)]: {
      fontSize: "1.1em"
    },
    [theme.breakpoints.down(339)]: {
      fontSize: "0.9em"
    }
  },
  mainTitle: {},
  subTitle: { marginTop: "10px" },

  imageSlieder: {
    marginTop: "50px",
    alignItems: "center",
    justifyContent: "center",
    // display:'flex',
    width: "100%"
  },

  Gallery: {
    display: "flex",
    flexDirection: "column",
    marginTop: 30,
    width: "100%"
  },

  galleryName_father: {
    width: "100%"
  },

  galleryName: {
    fontSize: "1.7em"
  },
  sectionsFather: {
    display: "grid",
    gridTemplateColumns: "repeat(4 , minmax(220px, 1fr))",
    [theme.breakpoints.down(900)]: {
      gridTemplateColumns: "repeat(2, minmax(170px, 1fr))"
    },
    justifyContent: "center",
    alignItems: "center",
    marginTop: "30px"
  },
  nameSection: {
    display: "flex",
    flexDirection: "column",
    margin: "10px 0px",
    width: "100%",
    alignItems: "center"
  },

  imageSections: {
    width: 150,
    height: 150
  },

  img: {
    width: "100%",
    height: "100%",
    border: "2px solid black"
  },

  button: {
    margin: theme.spacing(1),
    paddingTop: 0,
    paddingBottom: 0,
    fontSize: "1.5em",
    border: "1px solid black"
  },
  nameTypes: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: "10px"
  },
  articles: {
    display: "flex",
    width: "100%",
    marginTop: "30px"
  },
  articlesTitle: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    width: "100%"
  },
  articlesType: {
    display: "grid",
    gridTemplateColumns: "repeat(auto-fit , minmax(300px, 1fr));",
    gridGap: 15,
    alignItems: "center",
    justifyContent: "center",
    marginTop: "30px",
    width: "100%"
  },

  progress: {
    margin: theme.spacing(2)
  }
}));

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    width: state.navbar.resize.width,
    height: state.navbar.resize.height,
    blogs: state.blog.blogs
  };
};

const mapDispatchToProps = (dispatch: any) => ({
  getBlogs: (page: any, per_page: any) => dispatch(getBlogs(page, per_page))
});

export default compose(connect(mapStateToProps, mapDispatchToProps))(Home);
