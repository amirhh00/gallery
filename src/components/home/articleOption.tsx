import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { makeStyles } from "@material-ui/core/styles";
import {
  Box,
  Typography,
} from "@material-ui/core";
import CardMedia from "@material-ui/core/CardMedia";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import CardActions from "@material-ui/core/CardActions";
import { Link } from "react-router-dom";

const ArticleOption = (props: any) => {
  const classes = useStyles(props);
  function createMarkup() {
    return {__html: props.data.body};
  }
  return (
    <Box className={classes.cardfather}>
      <Card className={classes.card}>
        <CardActionArea>
          <CardMedia
            image={props.data.BlogImage}
            title="Contemplative Reptile"
            component="img"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              {props.data.title}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="div" dangerouslySetInnerHTML={createMarkup()} >
              
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
        <Link to="/article/:id" style={{ textDecoration: "none" }}>
          <Button size="medium" color="primary">
            {props.language ?"ادامه مطلب" : "more"}
          </Button>
          </Link>
        </CardActions>
      </Card>
    </Box>
  );
};

const useStyles = makeStyles((theme: any) => ({
  card: {
    // display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    width: "100%",
    height: "100%",
    boxShadow: "rgba(0, 0, 0, 0.6) 5px 3px 30px",
    // maxWidth: "500px"
  },

  cardfather: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    flexWrap: "wrap",
    maxWidth: "500px",
    [theme.breakpoints.down("669")]: {
      margin: "23px 0px"
    }
  }
}));

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    width: state.navbar.resize.width,
    height: state.navbar.resize.height
  };
};

export default compose(connect(mapStateToProps))(ArticleOption);
