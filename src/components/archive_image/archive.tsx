import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { makeStyles } from "@material-ui/core/styles";
import { Box, Typography } from "@material-ui/core";

import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import ImageGallery from "react-image-gallery";
import "react-image-gallery/styles/scss/image-gallery.scss";

const Archives = (props: any) => {
  const classes = useStyles(props);

  return (
    <Box className={classes.cardfather}>
      <Card className={classes.card}>
        <CardActionArea>
          <ImageGallery showThumbnails={false} items={props.img} />
          <CardContent>
            <Box className={classes.date}>
              <Typography
                gutterBottom
                variant="h6"
                component="h2"
                style={{ fontWeight: 500 }}
              >
                {props.name} - تاریخ: {props.date}
              </Typography>
            </Box>
          </CardContent>
        </CardActionArea>
      </Card>
    </Box>
  );
};

const useStyles = makeStyles((theme: any) => ({
  card: {
    // display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    width: "100%",
    height: "100%",
    boxShadow: "rgba(0, 0, 0, 0.6) 5px 3px 30px",
    maxWidth: "500px"
  },

  cardfather: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    flexWrap: "wrap",
    maxWidth: "500px",
    marginBottom: "40px",
    [theme.breakpoints.down("669")]: {
      margin: "23px 0px"
    }
  },
  date: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "15px"
  }
}));

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    width: state.navbar.resize.width,
    height: state.navbar.resize.height
  };
};

export default compose(connect(mapStateToProps))(Archives);
