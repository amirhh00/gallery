import React, { useState } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { makeStyles } from "@material-ui/core/styles";
import { Box, Typography, Container, CircularProgress } from "@material-ui/core";
import tjson from "./temp.json";
import Archives from "./archive";





const Archive = (props: any) => {
  const [jsonstate, setjsonstate] = useState<Array<any>>([]);
  const classes = useStyles(props);
  const params = new URLSearchParams(props.location.search);
  console.log('id:',params.get("id"));
  if (jsonstate.length > 0) {
  return (
    <Container maxWidth="lg" className={classes.container}>
      <Box className={classes.mainTitle}>

          <Box className={classes.typoImage_father}>
              <Typography style={{fontSize:'2em', fontWeight:'bold'}}>
              {props.language ?"دسته بندی طبیعت" : "landscape category"}
              </Typography>


          </Box>


      </Box>


      <Box className={classes.imagesFather}>

          
      {jsonstate.map((value, index) => {
            return <Archives key={index} img={value.img} name={value.name} date={value.date} />;
          })}



      </Box>
      

    </Container>
  );  } else {
    setjsonstate(tjson.data);
    return (
      <div>
        <CircularProgress className={classes.progress} />
      </div>
    );
  }
};

const useStyles = makeStyles((theme: any) => ({
  container: {
    display: "flex",
    // justifyContent: "center",
    // alignItems: "center",
    width: "100%",
    // borderLeft: "5px solid black",
    // borderRight: "5px solid black",
  },
  mainTitle:{
    display:'flex',
    flexDirection:'column',
    width:'100%',
    alignItems:'center',
    marginTop:'50px'


  },
  imagesFather:{
    display:'grid',
    gridTemplateColumns:"repeat(auto-fit , minmax(300px, 500px));",
    gridGap:15,
    width:'100%',
    alignItems:'center',
    justifyContent:'center',
    marginTop:'50px'
  },
  typoImage_father:{
      justifyContent:'center',
    width:'100%'

  },
  progress: {
    margin: theme.spacing(2)
  }
  

}));

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    width: state.navbar.resize.width,
    height: state.navbar.resize.height
  };
};

export default compose(connect(mapStateToProps))(Archive);
