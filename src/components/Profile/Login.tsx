import React, { useState, useEffect, useRef } from "react";
import { connect, useDispatch } from "react-redux";
import { compose } from "redux";
import { makeStyles } from "@material-ui/core/styles";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { Box, FormControl, Button, Typography, TextField } from "@material-ui/core";
import { loginAction } from "./redux/action";
import { withRouter } from "react-router-dom";

const Login = (props: any) => {
  const classes = useStyles(props);
  const [open, setOpen] = React.useState(false);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [submitError, setSubmitError] = useState("");
  const dispatch = useDispatch();
  let isMounted = useRef(true);

  useEffect(() => {
    return () => {
      isMounted.current = false;
    };
  }, []);

  const openSnack = () => {
    setOpen(true);
  };

  const handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();
    const emailInput: HTMLInputElement | null = document.querySelector("#email-input");
    const passwordInput: HTMLInputElement | null = document.querySelector("#password-input");
    if (emailInput !== null && passwordInput !== null) {
      if (emailInput.value === "" || passwordInput.value === "") {
        setSubmitError(props.language ? "ایمیل یا رمز عبور خالی است!" : "email & password is empty!");
        openSnack();
        emailInput.value === "" ? emailInput.focus() : passwordInput.focus();
      } else if (email !== "" && password !== "") {
        if (emailError === "" && passwordError === "") {
          dispatch(loginAction(email, password, props, setSubmitError));
          if (isMounted.current) openSnack();
          setTimeout(() => {
            if (isMounted.current) setSubmitError("");
          }, 6000);
        }
      }
    }
  };

  const handleChangeEmail = (value: any) => {
    if (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(value)) {
      setEmailError("");
      setEmail(value);
    } else {
      setEmailError(props.language ? "فرمت ایمیل اشتباه است!" : "your email is incorrect");
    }
  };

  const handleChangePassword = (value: any) => {
    if (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,32}$/.test(value)) {
      setPasswordError("");
      setPassword(value);
    } else {
      setPasswordError(props.language ? " پسورد باید حداقل دارای 8 کاراکتر حاوی حروف بزرگ انگلیسی و اعداد باشد !" : "your password is incorrect");
    }
  };

  return (
    <>
      <Box className={classes.title}>
        <Typography style={{ fontSize: "1.8em" }}>{props.language ? "ورود به حساب کاربری" : "Login"}</Typography>
      </Box>
      <form className={classes.form} onSubmit={handleSubmit}>
        <FormControl className={classes.inputFormControl}>
          <TextField
            error={emailError !== "" ? true : false}
            type="text"
            defaultValue={email}
            onChange={e => handleChangeEmail(e.target.value)}
            style={{ minWidth: "40%" }}
            id="email-input"
            label={props.language ? "آدرس ایمیل" : "Email Address"}
            variant="outlined"
          />
          <Typography style={{ color: "red" }}>{emailError}</Typography>
        </FormControl>

        <FormControl className={classes.inputFormControl}>
          <TextField
            error={passwordError !== "" ? true : false}
            type="password"
            defaultValue={password}
            onChange={e => handleChangePassword(e.target.value)}
            style={{ minWidth: "40%" }}
            id="password-input"
            label={props.language ? "رمز عبور" : "Password"}
            variant="outlined"
          />
          <Typography style={{ color: "red" }}>{passwordError}</Typography>
        </FormControl>

        <Box className={classes.btn}>
          <Button variant="contained" color="primary" type="submit" className={classes.btnSubmit}>
            {props.language ? "ورود" : "login"}
          </Button>
        </Box>
      </form>

      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left"
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        ContentProps={{
          "aria-describedby": "message-id"
        }}
        message={<span id="message-id">{submitError}</span>}
        action={[
          <IconButton key="close" aria-label="close" color="inherit" onClick={handleClose}>
            <CloseIcon />
          </IconButton>
        ]}
      />
    </>
  );
};

const useStyles = makeStyles((theme: any) => ({
  form: {
    display: "flex",
    flexDirection: "column",
    minWidth: "30%",
    maxWidth: "70%",
    alignItems: "center",
    marginTop: "20px"
  },
  btnSubmit: {
    maxWidth: "50%"
  },
  inputFormControl: {
    width: "100%",
    marginTop: "15px"
  },
  title: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignContent: "center",
    marginTop: 30
  },
  btn: {
    marginTop: 15
  }
}));

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    width: state.navbar.resize.width,
    height: state.navbar.resize.height
  };
};

export default compose(connect(mapStateToProps))(withRouter(Login));
