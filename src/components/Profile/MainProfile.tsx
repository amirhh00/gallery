import React, { useState } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { makeStyles } from "@material-ui/core/styles";
import Login from "./Login";
import Register from "./register";
import MiniDrawer from "./../userPanel";
import { Button, Container, Box } from "@material-ui/core";

const MainProfile = (props: any) => {
  const classes = useStyles(props);
  const [isLoginComponent, setIsLoginComponent] = useState(true);

  if (props.auth.isLogged === false) {
    if (isLoginComponent) {
      return (
        <Container maxWidth="lg" className={classes.container}>
          <Login />
          <br />
          <Box>
            <Button
              style={{
                marginRight: props.language ? 0 : 20,
                marginLeft: props.language ? 20 : 0
              }}
              color="secondary"
              onClick={() => setIsLoginComponent(false)}
            >
              {props.language ? "ساخت حساب کاربری جدید" : "Create New Account"}
            </Button>
            <Button color="secondary">{props.language ? "رمز عبور را فراموش کردم!" : "Forgot my password!"}</Button>
          </Box>
        </Container>
      );
    } else {
      return (
        <Container maxWidth="lg" className={classes.container}>
          <Register /> <br />
          <Button style={{ marginTop: 20 }} color="secondary" onClick={() => setIsLoginComponent(true)}>
            {props.language ? "حساب کاربری دارید؟ وارد شوید :)" : "Already have an account? Log in :)"}
          </Button>
        </Container>
      );
    }
  } else {
    return <MiniDrawer />;
  }
};

const useStyles = makeStyles((theme: any) => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    width: "100%",
    // borderLeft: "5px solid black",
    // borderRight: "5px solid black",
    marginTop: 30
  }
}));

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    width: state.navbar.resize.width,
    height: state.navbar.resize.height,
    auth: state.auth.auth
  };
};

export default compose(connect(mapStateToProps))(MainProfile);
