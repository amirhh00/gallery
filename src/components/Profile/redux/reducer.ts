import { combineReducers } from "redux";

const initialState = {
  token:"",
  isLogged:false,
  isAdmin:false
};

const auth = (state = initialState, action:any) => {
  switch (action.type) {
    case "LOGIN_USER": {
      const { data, isAdmin } = action.payload;
      return {
        ...state,
        token:data,
        isLogged:true,
        isAdmin
      };
    }

    case "REGISTER_USER": {
      const { data } = action.payload;
      return {
        ...state,
        token:data,
        isLogged:true,
        isAdmin:false
      };
    }

    case "LOGOUT_USER": {
      return {
        ...state,
        token:"",
        isLogged:false,
        isAdmin:false
      };
    }
    
    default:
      return state;
  }
};

export default combineReducers({ auth });
