import axios from "axios";

export const loginAction = (email = "", password = "", props: any, setSubmitError: any) => async (dispatch: any) => {
  try {
    let response = await axios({
      url: `${process.env.REACT_APP_DOMAIN}/v1/authenticate`,
      data: { email: email, password },
      method: "post"
    });
    if (response.data.success) {
      setSubmitError(props.language ? "ورود با موفقیت انجام شد!" : "login was successful!");
      setTimeout(() => {
        props.history.push("/");
        dispatch({
          type: "LOGIN_USER",
          payload: response.data
        });
      }, 1000);
    }
  } catch (err) {
    console.log(err.response);
    if (err.response.data.message === "Not authorized") {
      // password is wrong
      setSubmitError(props.language ? "کاربر  با این مشخصات وجود ندارد" : "User not found!");
    } else if (err.response.data.message === "User not found") {
      // email(or password) is wrong
      setSubmitError(props.language ? "کاربر  با این مشخصات وجود ندارد" : "User not found!");
    } else {
      setSubmitError(props.language ? "مشکلی در ارتباط با سرور وجود دارد" : "problem connecting to server!");
      console.log(err);
    }
  }
};

export const registerAction = (firstName: string, lastName: string, email: string, password: string, props: any, setSubmitError: any) => async (
  dispatch: any
) => {
  try {
    let response = await axios({
      url: `${process.env.REACT_APP_DOMAIN}/v1/register`,
      method: "post",
      data: {
        name: firstName,
        lastName: lastName,
        email: email,
        password: password
      }
    });
    if (response.data.success) {
      setSubmitError(props.language ? "ثبت نام با موفقیت انجام شد!" : "registration has been successfully completed!");
      setTimeout(() => {
        props.history.push("/");
        dispatch({
          type: "REGISTER_USER",
          payload: response.data
        });
      }, 2000);
    }
  } catch (err) {
    if (err.response.data.message === "user_already_exists") {
      setSubmitError(props.language ? "کاربر قبلا با این ایمیل ثبت نام شده" : "user already registered with this email!");
    } else {
      setSubmitError(props.language ? "مشکلی در ارتباط با سرور وجود دارد" : "problem connecting to server!");
      console.log(err);
    }
  }
};

export const checkToken = (token: string) => async (dispatch: any) => {
  try {
    let response = await axios({
      url: `${process.env.REACT_APP_DOMAIN}/V1/check-token`,
      headers: { authoratiosnjan: token },
      method: "GET"
    });
    if (response.data.success) {
      dispatch({
        type: "CHECK_TOKEN",
        payload: response.data
      });
    } else {
      console.log("response", response);
    }
  } catch (err) {
    console.log(err);
  }
};

export const logout = () => ({
  type: "LOGOUT_USER"
});
