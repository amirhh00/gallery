import React, { useState, useEffect, useRef } from "react";
import { connect, useDispatch } from "react-redux";
import { compose } from "redux";
import { withRouter } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import { Box, Typography, TextField, Button, IconButton } from "@material-ui/core";
import Snackbar from "@material-ui/core/Snackbar";
import CloseIcon from "@material-ui/icons/Close";
import { registerAction } from "./redux/action";

const Register = (props: any) => {
  const classes = useStyles(props);
  const [open, setOpen] = React.useState(false);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [rePassword, setRePassword] = useState("");
  const [firstNameError, setFirstNameError] = useState("");
  const [lastNameError, setLastNameError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [rePasswordError, setRePasswordError] = useState("");
  const [submitError, setSubmitError] = useState("");
  const dispatch = useDispatch();
  let isMounted = useRef(true);

  useEffect(() => {
    return () => {
      isMounted.current = false;
    };
  }, []);

  const openSnack = () => {
    setOpen(true);
  };

  const handleClose = (event: React.SyntheticEvent | React.MouseEvent, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    const firstNames: HTMLInputElement | null = document.querySelector("#firstNames");
    const lastNames: HTMLInputElement | null = document.querySelector("#lastNames");
    const emails: HTMLInputElement | null = document.querySelector("#emails");
    const passwords: HTMLInputElement | null = document.querySelector("#passwords");
    const rePasswords: HTMLInputElement | null = document.querySelector("#rePasswords");
    if (firstNames !== null && lastNames !== null && emails !== null && passwords !== null && rePasswords !== null) {
      if (firstNames.value === "" || lastNames.value === "" || emails.value === "" || passwords.value === "" || rePasswords.value === "") {
        setSubmitError(props.language ? "تمامی مقادیر را بنویسید!" : "fill all of the blanks!");
        openSnack();
        return;
      } else if (firstName === "" && lastName === "" && email === "" && password === "" && rePassword === "") {
        setSubmitError(props.language ? "تمامی مقادیر را صحیح  بنویسید!" : "fill all of the blanks correctly!");
        openSnack();
        return;
      } else if (firstNameError === "" && lastNameError === "" && emailError === "" && passwordError === "" && rePasswordError === "") {
        dispatch(registerAction(firstName, lastName, email, password, props, setSubmitError));
        if (isMounted.current) openSnack();
        setTimeout(() => {
          if (isMounted.current) setSubmitError("");
        }, 6000);
      } else {
        setSubmitError(props.language ? "خطا ها تصحیح کنید!" : "resolve the errors!");
        openSnack();
      }
    }
  };

  const handleChangeFirstName = (value: any) => {
    if (!/[0-9]+$/.test(value)) {
      setFirstNameError("");
      setFirstName(value);
    } else {
      setFirstNameError(props.language ? "!عدد نمیتواند وارد شود" : "Can't Accept Numbers!");
    }
  };
  const handleChangeLastName = (value: any) => {
    if (!/[0-9]+$/.test(value)) {
      setLastNameError("");
      setLastName(value);
    } else {
      setLastNameError(props.language ? "!عدد نمیتواند وارد شود" : "Can't Accept Numbers!");
    }
  };

  const handleChangeEmail = (value: any) => {
    if (/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(value)) {
      setEmailError("");
      setEmail(value);
    } else {
      setEmailError(props.language ? "فرمت ایمیل اشتباه است!" : "your email is incorrect");
    }
  };
  const handleChangePassword = (value: any) => {
    if (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,32}$/.test(value)) {
      setPasswordError("");
      setPassword(value);
    } else {
      setPasswordError(props.language ? " پسورد باید حداقل دارای 8 کاراکتر حاوی حروف بزرگ انگلیسی و اعداد باشد !" : "your password is incorrect");
    }
  };
  const handleChangeRePassword = (value: any) => {
    if (value === password) {
      setRePasswordError("");
      setRePassword(value);
    } else {
      setRePasswordError(props.language ? " مقدار وارد شده همخوانی ندارد!" : "the value is not match with Password!");
    }
  };

  return (
    <>
      <Box className={classes.title}>
        <Typography style={{ fontSize: "1.8em" }}>{props.language ? "ایجاد حساب کاربری" : "Create Acount"}</Typography>
      </Box>
      <Box className={classes.profileFields}>
        <form onSubmit={handleSubmit}>
          <Box className={classes.Fields}>
            <TextField
              error={firstNameError !== "" ? true : false}
              value={firstName}
              onChange={e => handleChangeFirstName(e.target.value)}
              style={{ minWidth: "40%" }}
              id="firstNames"
              label={props.language ? "نام" : "FirstName"}
              variant="outlined"
            />

            <Typography style={{ color: "red" }}>{firstNameError}</Typography>
          </Box>

          <Box className={classes.Fields}>
            <TextField
              error={lastNameError !== "" ? true : false}
              value={lastName}
              onChange={e => handleChangeLastName(e.target.value)}
              style={{ minWidth: "40%" }}
              id="lastNames"
              label={props.language ? "نام خانوادگی" : "Lastname"}
              variant="outlined"
            />

            <Typography style={{ color: "red" }}>{lastNameError}</Typography>
          </Box>

          <Box className={classes.Fields}>
            <TextField
              error={emailError !== "" ? true : false}
              defaultValue={email}
              onChange={e => handleChangeEmail(e.target.value)}
              style={{ minWidth: "40%" }}
              id="emails"
              label={props.language ? "ایمیل" : "Email"}
              variant="outlined"
            />

            <Typography style={{ color: "red" }}>{emailError}</Typography>
          </Box>

          <Box className={classes.Fields}>
            <TextField
              error={passwordError !== "" ? true : false}
              type="password"
              defaultValue={password}
              onChange={e => handleChangePassword(e.target.value)}
              style={{ minWidth: "40%" }}
              id="passwords"
              label={props.language ? "رمز عبور" : "Password"}
              variant="outlined"
            />

            <Typography style={{ color: "red" }}>{passwordError}</Typography>
          </Box>

          <Box className={classes.Fields} style={{}}>
            <TextField
              error={rePasswordError !== "" ? true : false}
              type="password"
              defaultValue={rePassword}
              onChange={e => handleChangeRePassword(e.target.value)}
              style={{ minWidth: "40%" }}
              id="rePasswords"
              disabled={password === "" ? true : false}
              label={props.language ? "تکرار رمز عبور" : "repeat the Password"}
              variant="outlined"
            />

            <Typography style={{ color: "red" }}>{rePasswordError}</Typography>
          </Box>

          <Box className={classes.Fields}>
            <Button type="submit" style={{ width: "100px" }} variant="contained" color="primary">
              {props.language ? "ثبت نام" : "Sign Up"}
            </Button>
          </Box>
        </form>
      </Box>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left"
        }}
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        ContentProps={{
          "aria-describedby": "message-id"
        }}
        message={<span id="message-id">{submitError}</span>}
        action={[
          <IconButton key="close" aria-label="close" color="inherit" onClick={handleClose}>
            <CloseIcon />
          </IconButton>
        ]}
      />
    </>
  );
};

const useStyles = makeStyles((theme: any) => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    width: "100%",
    borderLeft: "5px solid black",
    borderRight: "5px solid black",
    marginTop: 30
  },
  title: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignContent: "center"
  },

  form: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  profileFields: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    marginTop: "30px"
  },
  Fields: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    marginTop: "15px"
  }
}));

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    width: state.navbar.resize.width,
    height: state.navbar.resize.height
  };
};

export default compose(connect(mapStateToProps))(withRouter(Register));
