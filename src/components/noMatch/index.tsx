import React from "react";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import GIf from "./giphy-downsized-large.gif";
import { Container } from "@material-ui/core";

const noMatch = (props: any) => {
  console.log("props : ", props);
  return (
    <Container maxWidth={"lg"}>
      <div
        style={{
          width: "100vw",
          height: "80vh",
          background: `url(${GIf}) no-repeat center`,
          backgroundSize: "100% 100%",
          // zIndex: 999999,
          display: "flex",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <div
          style={{
            display: "flex",
            alignSelf: "center",
            justifyContent: "center",
            flexDirection: "column",
            alignItems: "center",
            textShadow: "2px 0 0 #fff, -2px 0 0 #fff, 0 2px 0 #fff, 0 -2px 0 #fff, 1px 1px #fff, -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff"
          }}
        >
          <Typography variant="h1">404</Typography>
          <br />
          <Typography variant="h4">{props.language ? "صفحه مورد نظر شما پیدا نشد" : "page not found"}</Typography>
        </div>
      </div>
    </Container>
  );
};

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    width: state.navbar.resize.width
  };
};

export default connect(mapStateToProps)(noMatch);
