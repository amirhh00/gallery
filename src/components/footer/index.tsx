import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { makeStyles } from "@material-ui/core/styles";
import { Box, Typography, Container } from "@material-ui/core";

import telegram from "./assets/telegram.png";
import instagram from "./assets/instagram.png";

const Footer = (props: any) => {
  const classes = useStyles(props);

  return (
    <Container maxWidth="lg" className={classes.container}>
      <Box className={classes.footer}>
        <Box className={classes.telInsta_father}>
          <Box className={classes.instagram_father}>
            <Box className={classes.image}>
              <img
                src={instagram}
                alt="instagram_logo"
                style={{ width: "100%", height: "100%" }}
              />
            </Box>
            <Box className={classes.adress}>
              <Typography style={{ fontSize: "1.1em", fontWeight: "bold" }}>
                instagram.com/aminphoto
              </Typography>
            </Box>
          </Box>

          <Box className={classes.telegram_father}>
            <Box className={classes.image}>
              <img
                src={telegram}
                alt="telegram_logo"
                style={{ width: "100%", height: "100%" }}
              />
            </Box>

            <Box className={classes.adress}>
              <Typography style={{ fontSize: "1.1em", fontWeight: "bold" }}>
                t.me/aminphoto
              </Typography>
            </Box>
          </Box>
        </Box>

        <Box className={classes.rightNote}>
          <Typography style={{ fontSize: "1.1em", fontWeight: "bold" }}>
            کلیه حق و حقوق این سایت متعلق به امین کشاورزی می باشد و هرگونه کپی
            برداری بدون ذکر نام عکاس ممنوع می باشد.
          </Typography>
        </Box>
      </Box>
    </Container>
  );
};

const useStyles = makeStyles((theme: any) => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    width: "100%",
    // borderLeft: "5px solid black",
    // borderRight: "5px solid black",
    height: 183
    // position:'absolute',
    // bottom:0
  },

  footer: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "50px",
    borderTop: "5px solid black",
    width: "100%"
  },
  telInsta_father: {
    display: "flex",
    marginTop: "30px",
    marginBottom: "10px"
  },
  instagram_father: {
    display: "flex"
  },
  telegram_father: {
    display: "flex",
    marginLeft: "15px"
  },
  image: {
    width: "50px",
    height: "50px",
    marginRight: "15px",
    [theme.breakpoints.down(644)]: {
      width: "40px",
      height: "40px",
    },
    [theme.breakpoints.down(428)]: {
      width: "30px",
      height: "30px",
    },
  },
  adress: {
    display: "flex",
    alignItems: "center",
    [theme.breakpoints.down(471)]: {
      fontSize:'0.9em'
    },
    [theme.breakpoints.down(379)]: {
      fontSize:'0.7em'
    },
  },
  rightNote: {
    marginBottom: "15px",
    [theme.breakpoints.down(669)]: {
      fontSize:'0.9em'
    },
    [theme.breakpoints.down(613)]: {
      fontSize:'0.7em'
    },
    [theme.breakpoints.down(471)]: {
      fontSize:'0.6em'
    },
    [theme.breakpoints.down(407)]: {
      fontSize:'0.5em'
    },
    [theme.breakpoints.down(351)]: {
      fontSize:'0.4em'
    },
    [theme.breakpoints.down(330)]: {
      fontSize:'0.3em'
    },
  }
}));

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    width: state.navbar.resize.width,
    height: state.navbar.resize.height
  };
};

export default compose(connect(mapStateToProps))(Footer);
