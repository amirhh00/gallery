import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { makeStyles } from "@material-ui/core/styles";
import { Container, Box, Typography, Button } from "@material-ui/core";
import { Link } from "react-router-dom";



import news from "./assets/news.jpg";
import portrait from "./assets/portrait.jpg";
import industrial from "./assets/industrial.jpg";
import landScape from "./assets/landScape.jpg";

const Gallery = (props: any) => {
  const classes = useStyles(props);

  return (
    <Container maxWidth="lg" className={classes.container}>
      <Box className={classes.title}>
        <Typography style={{ fontSize: "2em", fontWeight: "bold" }}>
        {props.language ?"گالری عکس ها" : "Gallery"}
        </Typography>
      </Box>

      <Box className={classes.galleryElemnts}>
        <Box className={classes.images_father}>
          <Box className={classes.image_father}>
            <img
              src={landScape}
              alt="landScape"
              className={classes.imgChild}
            />
          </Box>

             
          <Box className={classes.typo_imageFather}>
          <Link to="/archive" style={{ textDecoration: "none" }}>
            <Button variant="outlined" className={classes.button}>
                  {props.language ? "طبیعت" : "landscape"}
                </Button>
                </Link>
                </Box>
             
         
        </Box>

        <Box className={classes.images_father}>
          <Box className={classes.image_father}>
            <img src={portrait} alt="portrait" className={classes.imgChild} />
          </Box>

          <Box className={classes.typo_imageFather}>
          <Link to="/archive" style={{ textDecoration: "none" }}>
            <Button variant="outlined" className={classes.button}>
              {props.language ? "پرتره" : "portrait"}
            </Button>
            </Link>
          </Box>
        </Box>

        <Box className={classes.images_father}>
          <Box className={classes.image_father}>
            <img src={news} alt="news" className={classes.imgChild} />
          </Box>

          <Box className={classes.typo_imageFather}>
          <Link to="/archive" style={{ textDecoration: "none" }}>
            <Button variant="outlined" className={classes.button}>
              {props.language ? "خبری" : "news"}
            </Button>
            </Link>
          </Box>
        </Box>

        <Box className={classes.images_father}>
          <Box className={classes.image_father}>
            <img
              src={industrial}
              alt="industrial"
              className={classes.imgChild}
            />
          </Box>

          <Box className={classes.typo_imageFather}>
          <Link to="/archive" style={{ textDecoration: "none" }}>
            <Button variant="outlined" className={classes.button}>
              {props.language ? "صنعتی" : "industrial"}
            </Button>
            </Link>
          </Box>
        </Box>
      </Box>
    </Container>
  );
};

const useStyles = makeStyles((theme: any) => ({
  container: {
    // display: "flex",
    justifyContent: "center",
    // alignItems: "center",
    // flexDirection: "column",
    width: "100%",
    // borderLeft: "5px solid black",
    // borderRight: "5px solid black"
  },
  title: {
    width: "100%",
    display: "flex",
    marginTop: "40px",
    marginBottom: "40px",
    marginLeft: "40px",
    [theme.breakpoints.down(736)]: {
      fontSize: "1em"
    },
  },
  galleryElemnts: {
    display: "grid",
    gridTemplateColumns:"repeat(2 , minmax(220px, 1fr))",
    [theme.breakpoints.down(471)]: {
      gridTemplateColumns:"repeat(2 , minmax(150px, 1fr))",
    },
    gridColumnGap: 10,
    gridRowGap: 40,
    width: "100%"
  },
  images_father: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  image_father: {
    width: "250px",
    height: "250px",
    [theme.breakpoints.down(820)]: {
      width: "200px",
      height: "200px",
    },
    [theme.breakpoints.down(502)]: {
      width: "150px",
      height: "150px",
    },
    [theme.breakpoints.down(342)]: {
      width: "120px",
      height: "120px",
    },
  },
  typo_imageFather: {
    display: "flex",
    justifyContent: "center",
    width: "100%",
    marginTop: "15px"
  },
  button: {
    margin: theme.spacing(1),
    paddingTop: 0,
    paddingBottom: 0,
    fontSize: "1.5em",
    border: "1px solid black",
  },
  imgChild: {
    border: "2px solid black",
    width: "100%",
    height: "100%",
    borderRadius: "50%"
  }
}));

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    width: state.navbar.resize.width,
    height: state.navbar.resize.height
  };
};

export default compose(connect(mapStateToProps))(Gallery);
