import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { makeStyles } from "@material-ui/core/styles";
import { Container, Box, Typography } from "@material-ui/core";

import me from "./assets/me.jpg";
import dot from "./assets/dot.png";

const AboutMe = (props: any) => {
  const classes = useStyles(props);

  return (
    <Container maxWidth="lg" className={classes.container}>
      <Box className={classes.mainTitle}>
        <Typography style={{ fontSize: "2em", fontWeight: "bold" }}>
          {props.language ? "درباره من" : "aboutMe"}
        </Typography>
      </Box>

      <Box className={classes.btmSection}>
        <Box className={classes.rightSection}>
          <Box className={classes.imageSection}>
            <img
              src={me}
              alt="pic of me"
              style={{ width: "100%", height: "100%", borderRadius: "50%" }}
            />
          </Box>
        </Box>

        <Box className={classes.explainSection}>
          <Box className={classes.explainsFather}>
            <Box className={classes.exp1}>
              <Box className={classes.dot}>
                <img
                  src={dot}
                  alt="dot"
                  style={{ width: "10px", height: "10px" }}
                />
              </Box>

              <Typography style={{ fontSize: "1.4em" }}>
                امین کشاورزی هستم فارق التحصیل در رشته مدیریت جهانگردی در
                دانشگاه علم و فرهنگ در مقطع کارشناسی
              </Typography>
            </Box>
            <br />
            <Box className={classes.exp2}>
              <Box className={classes.dot}>
                <img
                  src={dot}
                  alt="dot"
                  style={{ width: "10px", height: "10px" }}
                />
              </Box>

              <Typography style={{ fontSize: "1.4em" }}>
                علاقه مند به عکاسی در فیلد هایی مثل طبیعت ،پرتره ،خبری و صنعتی
              </Typography>
            </Box>
            <br />
            <Box className={classes.exp3}>
              <Box className={classes.dot}>
                <img
                  src={dot}
                  alt="dot"
                  style={{ width: "10px", height: "10px" }}
                />
              </Box>

              <Typography style={{ fontSize: "1.4em" }}>
                سابقه کار مرتبط با عکاسی حدود ۴سال و همکاری با ماهنامه افق شهر
                قدس و نشریه صنایع شیمیایی گام پارت
              </Typography>
            </Box>
            <br />
            <Box className={classes.exp4}>
              <Box className={classes.dot}>
                <img
                  src={dot}
                  alt="dot"
                  style={{ width: "10px", height: "10px" }}
                />
              </Box>

              <Typography style={{ fontSize: "1.4em" }}>
                طراح صفحاب وب و کار های گرافیکی
              </Typography>
            </Box>
            <br />
            <br />

            <Box className={classes.exp5}>
              <Box className={classes.dot}>
                <img
                  src={dot}
                  alt="dot"
                  style={{ width: "10px", height: "10px" }}
                />
              </Box>

              <Typography style={{ fontSize: "1.4em" }}>
                من در سال ۱۳۹۴ آموزش عکاسی را نزد استاد خوبم خانم مهناز مظاهری
                به اتمام رساندم <br />و در حوزه عکاسی پرتره و صنعتی وارد بازار
                کار شدم. <br />
                در حال حاضر به عنوان طراح فرانت وب و عکاس(خبرنگار)در ماهنامه افق
                شهرقدش مشغول بکار هستم.{" "}
              </Typography>
            </Box>
          </Box>
        </Box>
      </Box>
    </Container>
  );
};

const useStyles = makeStyles((theme: any) => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    width: "100%",
    // borderLeft: "5px solid black",
    // borderRight: "5px solid black"
  },
  mainTitle: {
    display: "flex",
    width: "100%",
    marginTop: "30px",
    marginLeft: "20px"
  },
  btmSection: {
    display: "flex",
    width: "100%",
    marginTop: "40px",
    [theme.breakpoints.down(987)]: {
      flexDirection: "column"
    }
  },
  rightSection: {
    display: "flex",
    width: "20%",
    justifyContent: "center",
    [theme.breakpoints.down(987)]: {
      width: "100%"
    }
  },
  imageSection: {
    width: 160,
    height: 160,
    borderRadius: "50%",
    border: "2px solid black",
    [theme.breakpoints.down(723)]: {
      width: 130,
      height: 130
    }
  },
  explainSection: {
    display: "flex",
    width: "80%",
    [theme.breakpoints.down(987)]: {
      width: "100%",
      marginTop: 30,
      justifyContent: "center",
      alignItems: "center"
    }
  },
  explainsFather: {
    display: "flex",
    flexDirection: "column",
    marginTop: 10
  },
  exp1: {
    display: "flex",
    [theme.breakpoints.down(792)]: {
      fontSize: "0.9em"
    },
    [theme.breakpoints.down(723)]: {
      fontSize: "0.7em"
    }
  },
  exp2: {
    display: "flex",
    marginLeft: "10px",
    [theme.breakpoints.down(987)]: {
      marginLeft: 0
    },
    [theme.breakpoints.down(792)]: {
      fontSize: "0.9em"
    },
    [theme.breakpoints.down(723)]: {
      fontSize: "0.7em"
    }
  },
  exp3: {
    display: "flex",
    marginLeft: "5px",
    [theme.breakpoints.down(987)]: {
      marginLeft: 0
    },
    [theme.breakpoints.down(792)]: {
      fontSize: "0.9em"
    },
    [theme.breakpoints.down(723)]: {
      fontSize: "0.7em"
    }
  },
  exp4: {
    display: "flex",
    marginLeft: "-20px",
    [theme.breakpoints.down(987)]: {
      marginLeft: 0
    },
    [theme.breakpoints.down(792)]: {
      fontSize: "0.9em"
    },
    [theme.breakpoints.down(723)]: {
      fontSize: "0.7em"
    }
  },
  exp5: {
    display: "flex",
    [theme.breakpoints.down(792)]: {
      fontSize: "0.9em"
    },
    [theme.breakpoints.down(723)]: {
      fontSize: "0.7em"
    }
  },
  dot: {
    marginTop: "5px",
    marginRight: "10px"
  }
}));

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    width: state.navbar.resize.width,
    height: state.navbar.resize.height
  };
};

export default compose(connect(mapStateToProps))(AboutMe);
