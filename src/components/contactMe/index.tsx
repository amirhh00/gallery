import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { makeStyles } from "@material-ui/core/styles";
import { Container, Box, Typography } from "@material-ui/core";

import telegram from "./assets/telegram.png";
import instagram from "./assets/instagram.png";
import email from "./assets/email.png";

const ContactMe = (props: any) => {
  const classes = useStyles(props);

  return (
    <Container maxWidth="lg" className={classes.container}>
      <Box className={classes.mainTitle}>
        <Typography style={{ fontSize: "2em", fontWeight: "bold" }}>
          تماس با من
        </Typography>
      </Box>

      <Box className={classes.explainTypo}>
        <Typography style={{ fontSize: "1.5em", fontWeight: 400 }}>
          از طریق راه های ارتباطی زیر می تونید با بنده در ارتباط باشین. :)
        </Typography>
      </Box>

      <Box className={classes.socialMedia}>
        <Box className={classes.sections} style={{marginBottom:'18px'}}>
          <Box className={classes.imageFather}>
            <img
              src={telegram}
              alt="telegram"
              style={{ width: "100%", height: "100%" }}
            />
          </Box>
          <Box className={classes.adress}>
            <Typography style={{ fontSize: "1.3em" }}>
              t.me/aminphoto
            </Typography>
          </Box>
        </Box>
        <Box className={classes.sections}>

          <Box className={classes.imageFather}>

            <img
              src={instagram}
              alt="instagram"
              style={{ width: "100%", height: "100%" }}
            />
          </Box>
          <Box className={classes.adress}>
            <Typography style={{ fontSize: "1.3em" }}>
              instagram.com/aminphoto
            </Typography>
          </Box>
        </Box>
        <Box className={classes.sections}>
          <Box className={classes.imageFather}>
            <img
              src={email}
              alt="email"
              style={{ width: "100%", height: "100%" }}
            />
          </Box>
          <Box className={classes.adress}>
            <Typography style={{ fontSize: "1.3em" }}>
                aminkeshavarzi5@gmail.com
            </Typography>
          </Box>
        </Box>
      </Box>
    </Container>
  );
};

const useStyles = makeStyles((theme: any) => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    width: "100%",
    // borderLeft: "5px solid black",
    // borderRight: "5px solid black"
  },
  mainTitle: {
    display: "flex",
    width: "100%",
    marginTop:'40px',
    [theme.breakpoints.down(509)]: {
      fontSize:'0.8em'
    },
  },
  explainTypo: {
    display: "flex",
    width: "100%",
    marginTop:'30px',
    [theme.breakpoints.down(509)]: {
      fontSize:'0.8em'
    },
    [theme.breakpoints.down(413)]: {
      fontSize:'0.6em'
    },
  },
  socialMedia: {
    display: "flex",
    width: "100%",
    flexDirection: "column",
    marginTop:'40px',
    marginLeft:'30px'
  },
  sections: {
    display: "flex",
    width: "100%",
    marginBottom:'10px'
  },
  imageFather: {
    width: "54px",
    height: "54px",
    [theme.breakpoints.down(509)]: {
      width: "40px",
      height: "40px",
    },
  },
  adress:{
    display:'flex',
    alignItems:'center',
    justifyContent:'center',
    marginLeft:"10px",
    [theme.breakpoints.down(509)]: {
      fontSize:'0.8em'
    },

  },
}));

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    width: state.navbar.resize.width,
    height: state.navbar.resize.height
  };
};

export default compose(connect(mapStateToProps))(ContactMe);
