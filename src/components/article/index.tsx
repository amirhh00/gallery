import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { makeStyles } from "@material-ui/core/styles";
import { Container} from "@material-ui/core";


const Article = (props: any) => {
  const classes = useStyles(props);
  
 
  return (
    <Container maxWidth="lg" className={classes.container}>
    
      
        <h1>Hello</h1>    
          
          
    
    </Container>
  );

};


const useStyles = makeStyles((theme: any) => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    width: "100%",
    // borderLeft: "5px solid black",
    // borderRight: "5px solid black"
  },
 

}));

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    width: state.navbar.resize.width,
    height: state.navbar.resize.height
  };
};


export default compose(connect(mapStateToProps))(Article);
