import React, { useEffect } from "react";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import { Button } from "@material-ui/core";
import moment from "moment-jalaali";
import fa from "moment/locale/fa";
import queryString from "query-string";

import BlogEditor from "./BlogEditor";
import { getBlogs } from "./redux/action";
import "./style.scss";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardBody: {
    textAlign: "center",
    "& *": {
      textAlign: "center"
    }
  },
  tableWrapper: {
    maxHeight: 407,
    overflow: "auto"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);

const columns = [
  { id: "title", label: "عنوان", minWidth: 50, maxWidth: 75 },
  {
    id: "author",
    label: "نویسنده",
    minWidth: 40,
    maxWidth: 65,
    align: "right",
    format: input => (!input || input === "" ? "گمنام" : input)
  },
  {
    id: "body",
    label: "متن",
    minWidth: 120,
    align: "right",
    format: input => <div maxLength="20" className="dangerous" dangerouslySetInnerHTML={{ __html: input.replace(/<img .*?>/g, "") }} />
  },
  {
    id: "createdAt",
    label: "تاریخ ایجاد",
    minWidth: 120,
    align: "right",
    // format: value => value.toFixed(2)
    format: value => {
      moment.locale("fa", fa);
      const date = moment(value);
      const date1 = date.format("jYYYY/jM/jD");
      const date2 = date.fromNow();
      return `${date1}  ${date2}`;
    }
  },
  {
    id: "thumbnail",
    label: "تصویر",
    maxWidth: 120,
    align: "center",
    format: input => <img maxLength="20" alt={"blogThumbnail"} className="dangerousImg" src={input} />
  }
];

const Blog = props => {
  const classes = useStyles();
  const parsed = queryString.parse(props.location.search);
  const [page, setPage] = React.useState(parseInt(parsed.page, 10) || 1);
  // const prevpage = usePrevious(page);
  const [rowsPerPage, setRowsPerPage] = React.useState(parseInt(parsed.per_page, 10) || 10);
  const [rows, setRows] = React.useState([]);

  useEffect(() => {
    if (!parsed.id) {
      props.getBlogs(page, rowsPerPage);
    }
    // eslint-disable-next-line
  }, [page, rowsPerPage]);

  useEffect(() => {
    setRows([]);
    if (page && rowsPerPage && props.blogs && props.blogs.pages.length > 0 && props.blogs.pages[page - 1].length > 0) {
      props.blogs.pages[page - 1].map(value =>
        setRows(prev => [
          ...prev,
          { ID: value._id, title: value.title, author: value.author, body: value.body, createdAt: value.createdAt, thumbnail: value.BlogImage }
        ])
      );
    }
    // eslint-disable-next-line
  }, [props.blogs]);

  const handleChangePage = (_event, newPage) => {
    setPage(newPage + 1);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleClickSingleBlog = (ID, page, index) => {
    if (ID && page > 0 && typeof index === "number") {
      props.history.push({
        search: `?id=${ID}&page=${page - 1}&index=${index}&per_page=${rowsPerPage}` // should be data.id
      });
    } else {
      props.history.push({ search: `?id=${ID}` });
    }
  };

  if (parsed.id) {
    return <BlogEditor blogID={parsed.id} />;
  }
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Button onClick={() => handleClickSingleBlog()} variant="outlined">
          افزودن بلاگ جدید
        </Button>
        <Card plain>
          <CardHeader plain color="primary">
            <h4 className={classes.cardTitleWhite}>جدول بلاگ ها</h4>
            <p className={classes.cardCategoryWhite}>در صورت کلیک امکان ویرایش وجود دارد</p>
          </CardHeader>
          <CardBody className={classes.cardBody}>
            <div className={classes.tableWrapper}>
              <Table>
                <TableHead>
                  <TableRow>
                    {columns.map(column => (
                      <TableCell key={column.id} align={column.align} style={{ minWidth: column.minWidth }}>
                        {column.label}
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows
                    // .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row, index) => {
                      return (
                        <TableRow hover role="checkbox" tabIndex={-1} key={row.ID}>
                          {columns.map(column => {
                            const value = row[column.id];
                            return (
                              <TableCell onClick={() => handleClickSingleBlog(row.ID, page, index)} key={column.id} align={column.align}>
                                {column.format ? column.format(value) : value}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </div>
            <TablePagination
              rowsPerPageOptions={[5, 10, 50]}
              dir="ltr"
              component="div"
              count={props.blogs.blogsCount}
              rowsPerPage={rowsPerPage}
              page={page - 1}
              backIconButtonProps={{
                "aria-label": "previous page"
              }}
              nextIconButtonProps={{
                "aria-label": "next page"
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  );
};

const mapStateToProps = state => ({
  width: state.navbar.resize.width,
  blogs: state.blog.blogs
});

const mapDispatchToProps = dispatch => ({
  getBlogs: (page, per_page) => dispatch(getBlogs(page, per_page))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(React.memo(Blog));
