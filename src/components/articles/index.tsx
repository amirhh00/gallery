import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { makeStyles } from "@material-ui/core/styles";
import { Container, Box, Typography, CircularProgress, TablePagination } from "@material-ui/core";
import { getBlogs } from "../home/redux/action";



import StateBloger from "./stateBloger";

const Articles = (props: any) => {
  const [jsonstate, setjsonstate] = useState<Array<any>>([]);
  const classes = useStyles(props);
 
  const [page, setPage] = React.useState(6);
  // const prevpage = usePrevious(page);
  const [rowsPerPage, setRowsPerPage] = React.useState(6);
  // const [rows, setRows] = React.useState([]);

  useEffect(() => {
    if (6) {
      props.getBlogs(page, rowsPerPage);
    }
    // eslint-disable-next-line
  }, [page, rowsPerPage]);

  // useEffect(() => {
  //   setRows([]);
  //   if (page && rowsPerPage && props.blogs && props.blogs.pages.length > 0 && props.blogs.pages[page - 1].length > 0) {
  //     props.blogs.pages[page - 1].map(value =>
  //       setRows(prev => [
  //         ...prev,
  //         { ID: value._id, title: value.title, author: value.author, body: value.body, createdAt: value.createdAt, thumbnail: value.BlogImage }
  //       ])
  //     );
  //   }
  //   // eslint-disable-next-line
  // }, [props.blogs]);

  const handleChangePage = (_event: any, newPage: number) => {
    setPage(newPage + 1);
  };

  const handleChangeRowsPerPage = (event: { target: { value: React.ReactText; }; }) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  
  // useEffect(() => {
  //   props.getBlogs(1, 6);
  //   // eslint-disable-next-line
  // }, []);

  useEffect(() => {
    setjsonstate(props.blogs.pages[0]);
    // console.log('props.blogs.pages',props.blogs.pages);
  }, [props.blogs]);

  if (jsonstate.length > 0) {
  return (
    <Container maxWidth="lg" className={classes.container}>
      <Box className={classes.mainTitle}>
        <Typography style={{ fontSize: "2.3em", fontWeight: "bold" }}>
          مقالات
        </Typography>
      </Box>
      
            
      <Box className={classes.articlesType}>
          {jsonstate.map((value, index) => {
            return <StateBloger key={index} data={value} />;
          })}
        </Box>
        <TablePagination
              rowsPerPageOptions={[6]}
              dir={props.language ? "rtl" : "ltr"}
              component="div"
              count={props.blogs.blogsCount}
              rowsPerPage={rowsPerPage}
              page={page - 1}
              backIconButtonProps={{
                "aria-label": "previous page"
              }}
              nextIconButtonProps={{
                "aria-label": "next page"
              }}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />

    </Container>
  );
} 
else {
  return (
    <div>
      <CircularProgress className={classes.progress} />
    </div>
  );
}

};


const useStyles = makeStyles((theme: any) => ({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    width: "100%",
    // borderLeft: "5px solid black",
    // borderRight: "5px solid black"
  },
  mainTitle: {
    display: "flex",
    width: "100%",
    marginTop: "30px",
    
  },
  btmSection:{
    display:'flex',
    flexDirection:'column',
    marginTop:'30px'

  },
 
  explainSection: {
    display: "flex",
    flexDirection:'column',
    width: "80%"
  },
  explainsFather: {
    display: "flex",
    flexDirection: "column",
    marginTop: 10
  },
  exp1: {
    display: "flex"
  },
  exp5: {
    display: "flex"
  },
  imageFather:{
    width:'500px',
    height:'300px',
    border:'2px solid black',
    marginTop:'20px',
    marginBottom:'20px'


  },
  imageSection:{
    width:'100%',
    display:'flex',
    justifyContent:'center',
    alignItems:'center'

  },
  
  articlesType: {
    display: "grid",
    gridTemplateColumns: "repeat(auto-fit , minmax(300px, 1fr));",
    gridGap: 15,
    alignItems: "center",
    justifyContent: "center",
    marginTop: "30px",
    width: "100%"
  },
  
  progress: {
    margin: theme.spacing(2)
  }
}));

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    width: state.navbar.resize.width,
    height: state.navbar.resize.height,
    blogs: state.blog.blogs
  };
};

const mapDispatchToProps = (dispatch: any) => ({
  getBlogs: (page: any, per_page: any) => dispatch(getBlogs(page, per_page))
});


export default compose(connect(
  mapStateToProps,mapDispatchToProps))(Articles);
