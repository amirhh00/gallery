import { createStore, applyMiddleware } from "redux";
import { persistCombineReducers, persistReducer, persistStore } from "redux-persist";
import { composeWithDevTools } from "redux-devtools-extension";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web
import thunk from "redux-thunk";
import { createBrowserHistory as createHistory } from "history";
import { routerMiddleware } from "react-router-redux";

import navbar from "./components/navBar/redux/reducer";
import blog from "./components/home/redux/reducer";
import auth from './components/Profile/redux/reducer';

const rootPersistConfig = {
  key: "root",
  storage,
  blacklist: ["navbar"]
  // debug: true,
};

const rootReducer = persistCombineReducers(rootPersistConfig, {
  navbar: persistReducer(
    {
      key: "navbar",
      storage,
      blacklist: ["resize", "toggleDrawer"]
    },
    navbar
  ),
  blog: persistReducer(
    {
      key: "blog",
      storage,
      blacklist: ["blog"]
    },
    blog
  ),
  auth: persistReducer(
    {
      key: "auth",
      storage,
      blacklist: ["auth"]
    },
    auth
  )
});

const history = createHistory();

const store = createStore(rootReducer, undefined, composeWithDevTools(applyMiddleware(thunk, routerMiddleware(history))));

persistStore(store);

export { history };
export default store;
