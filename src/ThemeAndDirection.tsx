import React from "react";
import { connect } from "react-redux";
import { create } from "jss";
import rtl from "jss-rtl";
import { StylesProvider, jssPreset } from "@material-ui/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import { ThemeProvider } from "@material-ui/styles";
import { red } from "@material-ui/core/colors";
import { createMuiTheme, responsiveFontSizes } from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import json2mq from "json2mq";
import { isMobile } from "react-device-detect";
import Typography from "@material-ui/core/Typography";

import Routes from "./Routes";

import noise from "./assets/noise.png";
import biggerDevice from "./assets/biggerDevice.svg";
import rotateDevice from "./assets/screen-rotation-button.svg";


const ThemeAndDirection = (props: any) => {
  document.body.dir = props.direction;

  const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

  const landscp = useMediaQuery(
    json2mq({
      orientation: "landscape",
      minDeviceWidth: 480,
      maxDeviceWidth: 860
    })
  );

  const smallDevice = useMediaQuery(
    json2mq({
      maxDeviceWidth: 310
    })
  );

  let theme: any = createMuiTheme({
    direction: props.direction,
    // get site direction from redux
    props: {
      // Name of the component ⚛️
      MuiButtonBase: {
        // The default props to change
        disableRipple: false
      },
      MuiPaper: {
        elevation: 24
      }
    },
    palette: {
      type: props.themeColor ?'light' :'dark'  ,
      primary: {
        main: "#000"
      },
      secondary: {
        main: "#552828"
      },
      error: {
        main: red.A400
      },
      background: {
        default: props.themeColor ? "#FAF5EF" : 'black'
      }
      // set colors
    },
    typography: {
     
      fontFamily: ["iranyekan"].join(","),
      
      
    },
    // add iranyekan font

    overrides: {
      MuiFab: {
        root: {
          zIndex: 1,
          textTransform: "none"
        }
      },
      MuiContainer: {
        root: {
          display: "flex",
          flexWrap: "wrap"
        }
      },
      MuiPaper: {
        elevation4: {
          display: "none"
        }
      }

    }
    // change default styles of Material-ui
  });

  theme = responsiveFontSizes(theme);

  return (
    <div>
      <StylesProvider jss={jss}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          {landscp && isMobile ? (
            <div
              style={{
                width: "100vw",
                height: "100vh",
                backgroundImage: `url(${noise})`,
                position: "absolute",
                zIndex: 999999,
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <div
                style={{
                  display: "flex",
                  alignSelf: "center",
                  justifyContent: "center",
                  flexDirection: "column",
                  alignItems: "center"
                }}
              >
                <img alt="rotateDevice" src={rotateDevice} style={{ width: 130 }} />
                <br />
                <Typography variant="h5">{props.language ? "لطفا گوشی خود را بچرخانید" : "Please turn your device"}</Typography>
              </div>
            </div>
          ) : smallDevice ? (
            <div
              style={{
                width: "100vw",
                height: "100vh",
                backgroundImage: `url(${noise})`,
                position: "absolute",
                zIndex: 999999,
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <div
                style={{
                  display: "flex",
                  alignSelf: "center",
                  justifyContent: "center",
                  flexDirection: "column",
                  alignItems: "center"
                }}
              >
                <img alt="biggerDevice" src={biggerDevice} style={{ width: 80 }} />
                <br />
                <Typography variant="caption">{props.language ? "لطفا از دستگاه بزرگتری استفاده کنید!" : "Please use a bigger device!"}</Typography>
              </div>
            </div>
          ) : (
            <Routes />
          )}
        </ThemeProvider>
      </StylesProvider>
    </div>
  );
};

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  if (language === "FA") language = true;
  else language = false;

  return {
    direction: state.navbar.toggleLanguage.direction,
    language,
    themeColor:state.navbar.themeColor
  };
};

export default connect(mapStateToProps)(ThemeAndDirection);
