import React, { useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Loadable from "react-loadable";
// import { ClipLoader } from "react-spinners";
import { connect } from "react-redux";

// import {deviceSize} from './components/navBar/actions'
import NavBar from "./components/navBar";
import NoMatch from "./components/noMatch";

const Loading = () => {
  return (
    <div
      style={{
        height: "100%",
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        marginTop: "30vh"
      }}
    >
      {/* <ClipLoader
            sizeUnit={"px"}
            size={60}
            color={"#552828"}
            loading={true}
      /> */}
    </div>
  );
};

const Home = Loadable({
  loader: () => import("./components/home/"),
  loading: Loading
});
const Footer = Loadable({
  loader: () => import("./components/footer/"),
  loading: Loading
});

const Gallery = Loadable({
  loader: () => import("./components/gallery/"),
  loading: Loading
});

const Archive = Loadable({
  loader: () => import("./components/archive_image/"),
  loading: Loading
});
const ContactMe = Loadable({
  loader: () => import("./components/contactMe/"),
  loading: Loading
});
const AboutMe = Loadable({
  loader: () => import("./components/aboutMe/"),
  loading: Loading
});
const Articles = Loadable({
  loader: () => import("./components/articles/"),
  loading: Loading
});

const Article = Loadable({
  loader: () => import("./components/article/"),
  loading: Loading
});
const MainProfile = Loadable({
  loader: () => import("./components/Profile/MainProfile"),
  loading: Loading
});
// const Panel = Loadable({
//   loader: () => import("./components/userPanel"),
//   loading: Loading
// });

const Routes = (props: any) => {
  useEffect(()=>{
    // console.log('checkToken',props.token);
  },[])
  return (
    <Router>
      <NavBar />
      <div style={{ minHeight: "calc(100vh - 279px)" }}>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/gallery" component={Gallery} />
          <Route path="/archive" component={Archive} />
          <Route path="/contactMe" component={ContactMe} />
          <Route path="/aboutMe" component={AboutMe} />
          <Route path="/articles" component={Articles} />
          {/* <Route path="/panel" component={Panel} /> */}
          <Route path="/article/:id" component={Article} />
          <Route path="/profile" component={MainProfile} />
          <Route component={NoMatch} />
        </Switch>
      </div>

      <Footer />

      {/* <div style={{ height: 56, display: props.width > 900 ? 'none' : 'block' }} /> */}
    </Router>
  );
};

const mapStateToProps = (state: any) => {
  let language = state.navbar.toggleLanguage.language;
  let width = state.navbar.resize.width;
  if (language === "FA") language = true;
  else language = false;
  return {
    language,
    width,
    token:state.auth.auth.token
  };
};

export default connect(mapStateToProps)(React.memo(Routes));
