import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import store from "./configureStore";
import ThemeAndDirection from "./ThemeAndDirection";
import * as serviceWorker from "./serviceWorker";

// console.log('dondjun',process.env.REACT_APP_DOMAIN);

ReactDOM.render(
  <Provider store={store}>
    <ThemeAndDirection />
  </Provider>,
  document.querySelector("#root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
