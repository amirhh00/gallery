FROM node:12.13-alpine as react-build
WORKDIR /front
ENV PATH /front/node_modules/.bin:$PATH
COPY . ./
RUN npm i
RUN printenv > .env.production
RUN npm run build-prod

FROM nginx:alpine
COPY --from=react-build /front/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]